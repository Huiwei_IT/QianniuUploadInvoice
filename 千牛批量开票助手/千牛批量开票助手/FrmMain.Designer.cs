﻿namespace 千牛批量开票助手
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Obsolete]
        private void InitializeComponent()
        {
            this.BtnStop = new System.Windows.Forms.Button();
            this.BtnRun = new System.Windows.Forms.Button();
            this.TabCtrls = new System.Windows.Forms.TabControl();
            this.TabpList = new System.Windows.Forms.TabPage();
            this.DgvList = new System.Windows.Forms.DataGridView();
            this.FStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FOrderNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FAmountDetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FOLAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TabpSetting = new System.Windows.Forms.TabPage();
            this.GrpAuto = new System.Windows.Forms.GroupBox();
            this.NudLoopQty = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.NudLoopTime = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.GrpPath = new System.Windows.Forms.GroupBox();
            this.BtnOpenBackup = new System.Windows.Forms.Button();
            this.BtnOpenInvoice = new System.Windows.Forms.Button();
            this.BtnBackupPath = new System.Windows.Forms.Button();
            this.BtnInovicePath = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtBackupPath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtInvoicePath = new System.Windows.Forms.TextBox();
            this.GrpUsr = new System.Windows.Forms.GroupBox();
            this.BtnQrLogin = new System.Windows.Forms.Button();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CboStatus = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtPwd = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtUsr = new System.Windows.Forms.TextBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.SplWeb = new System.Windows.Forms.SplitContainer();
            this.WebL = new CefSharp.WinForms.ChromiumWebBrowser();
            this.WebR = new CefSharp.WinForms.ChromiumWebBrowser();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.LblListCount = new System.Windows.Forms.Label();
            this.ChbIdentifyStatus = new System.Windows.Forms.CheckBox();
            this.ChbDataLock = new System.Windows.Forms.CheckBox();
            this.ChbRunStatus = new System.Windows.Forms.CheckBox();
            this.TabCtrls.SuspendLayout();
            this.TabpList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvList)).BeginInit();
            this.TabpSetting.SuspendLayout();
            this.GrpAuto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NudLoopQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudLoopTime)).BeginInit();
            this.GrpPath.SuspendLayout();
            this.GrpUsr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplWeb)).BeginInit();
            this.SplWeb.Panel1.SuspendLayout();
            this.SplWeb.Panel2.SuspendLayout();
            this.SplWeb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnStop
            // 
            this.BtnStop.Location = new System.Drawing.Point(11, 88);
            this.BtnStop.Name = "BtnStop";
            this.BtnStop.Size = new System.Drawing.Size(157, 65);
            this.BtnStop.TabIndex = 0;
            this.BtnStop.Text = "停止\r\n(Ctrl+F12)";
            this.BtnStop.UseVisualStyleBackColor = true;
            this.BtnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // BtnRun
            // 
            this.BtnRun.Location = new System.Drawing.Point(11, 26);
            this.BtnRun.Name = "BtnRun";
            this.BtnRun.Size = new System.Drawing.Size(157, 56);
            this.BtnRun.TabIndex = 0;
            this.BtnRun.Text = "启动\r\n(Ctrl+F5)";
            this.BtnRun.UseVisualStyleBackColor = true;
            this.BtnRun.Click += new System.EventHandler(this.BtnRun_Click);
            // 
            // TabCtrls
            // 
            this.TabCtrls.Controls.Add(this.TabpList);
            this.TabCtrls.Controls.Add(this.TabpSetting);
            this.TabCtrls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabCtrls.Location = new System.Drawing.Point(0, 0);
            this.TabCtrls.Margin = new System.Windows.Forms.Padding(0);
            this.TabCtrls.Name = "TabCtrls";
            this.TabCtrls.SelectedIndex = 0;
            this.TabCtrls.Size = new System.Drawing.Size(1600, 240);
            this.TabCtrls.TabIndex = 1;
            // 
            // TabpList
            // 
            this.TabpList.Controls.Add(this.DgvList);
            this.TabpList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabpList.Location = new System.Drawing.Point(4, 26);
            this.TabpList.Name = "TabpList";
            this.TabpList.Padding = new System.Windows.Forms.Padding(3);
            this.TabpList.Size = new System.Drawing.Size(1592, 210);
            this.TabpList.TabIndex = 0;
            this.TabpList.Text = "发票队列";
            this.TabpList.UseVisualStyleBackColor = true;
            // 
            // DgvList
            // 
            this.DgvList.AllowUserToAddRows = false;
            this.DgvList.AllowUserToDeleteRows = false;
            this.DgvList.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FStatus,
            this.FQty,
            this.FOrderNum,
            this.FPath,
            this.FAmount,
            this.FAmountDetail,
            this.FOLAmount});
            this.DgvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvList.Location = new System.Drawing.Point(3, 3);
            this.DgvList.Name = "DgvList";
            this.DgvList.ReadOnly = true;
            this.DgvList.RowHeadersWidth = 4;
            this.DgvList.RowTemplate.Height = 23;
            this.DgvList.Size = new System.Drawing.Size(1586, 204);
            this.DgvList.TabIndex = 0;
            // 
            // FStatus
            // 
            this.FStatus.HeaderText = "状态";
            this.FStatus.MinimumWidth = 80;
            this.FStatus.Name = "FStatus";
            this.FStatus.ReadOnly = true;
            this.FStatus.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FStatus.Width = 80;
            // 
            // FQty
            // 
            this.FQty.HeaderText = "数量";
            this.FQty.MinimumWidth = 40;
            this.FQty.Name = "FQty";
            this.FQty.ReadOnly = true;
            this.FQty.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FQty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FQty.Width = 40;
            // 
            // FOrderNum
            // 
            this.FOrderNum.HeaderText = "订单编号";
            this.FOrderNum.MinimumWidth = 300;
            this.FOrderNum.Name = "FOrderNum";
            this.FOrderNum.ReadOnly = true;
            this.FOrderNum.Width = 300;
            // 
            // FPath
            // 
            this.FPath.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FPath.HeaderText = "发票路径";
            this.FPath.MinimumWidth = 260;
            this.FPath.Name = "FPath";
            this.FPath.ReadOnly = true;
            this.FPath.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FPath.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FAmount
            // 
            this.FAmount.HeaderText = "发票金额";
            this.FAmount.MinimumWidth = 80;
            this.FAmount.Name = "FAmount";
            this.FAmount.ReadOnly = true;
            this.FAmount.Width = 80;
            // 
            // FAmountDetail
            // 
            this.FAmountDetail.HeaderText = "发票金额明细";
            this.FAmountDetail.MinimumWidth = 160;
            this.FAmountDetail.Name = "FAmountDetail";
            this.FAmountDetail.ReadOnly = true;
            this.FAmountDetail.Width = 160;
            // 
            // FOLAmount
            // 
            this.FOLAmount.HeaderText = "线上金额";
            this.FOLAmount.MinimumWidth = 80;
            this.FOLAmount.Name = "FOLAmount";
            this.FOLAmount.ReadOnly = true;
            this.FOLAmount.Width = 80;
            // 
            // TabpSetting
            // 
            this.TabpSetting.Controls.Add(this.GrpAuto);
            this.TabpSetting.Controls.Add(this.GrpPath);
            this.TabpSetting.Controls.Add(this.GrpUsr);
            this.TabpSetting.Location = new System.Drawing.Point(4, 22);
            this.TabpSetting.Name = "TabpSetting";
            this.TabpSetting.Padding = new System.Windows.Forms.Padding(3);
            this.TabpSetting.Size = new System.Drawing.Size(1592, 214);
            this.TabpSetting.TabIndex = 1;
            this.TabpSetting.Text = "设置";
            this.TabpSetting.UseVisualStyleBackColor = true;
            // 
            // GrpAuto
            // 
            this.GrpAuto.Controls.Add(this.NudLoopQty);
            this.GrpAuto.Controls.Add(this.label2);
            this.GrpAuto.Controls.Add(this.NudLoopTime);
            this.GrpAuto.Controls.Add(this.label10);
            this.GrpAuto.Location = new System.Drawing.Point(538, 6);
            this.GrpAuto.Name = "GrpAuto";
            this.GrpAuto.Size = new System.Drawing.Size(261, 129);
            this.GrpAuto.TabIndex = 2;
            this.GrpAuto.TabStop = false;
            this.GrpAuto.Text = "自动化";
            // 
            // NudLoopQty
            // 
            this.NudLoopQty.Location = new System.Drawing.Point(99, 57);
            this.NudLoopQty.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NudLoopQty.Name = "NudLoopQty";
            this.NudLoopQty.Size = new System.Drawing.Size(144, 23);
            this.NudLoopQty.TabIndex = 3;
            this.NudLoopQty.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "轮询批次(个)";
            // 
            // NudLoopTime
            // 
            this.NudLoopTime.Location = new System.Drawing.Point(99, 28);
            this.NudLoopTime.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NudLoopTime.Name = "NudLoopTime";
            this.NudLoopTime.Size = new System.Drawing.Size(144, 23);
            this.NudLoopTime.TabIndex = 0;
            this.NudLoopTime.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 17);
            this.label10.TabIndex = 1;
            this.label10.Text = "轮询时间(秒)";
            // 
            // GrpPath
            // 
            this.GrpPath.Controls.Add(this.BtnOpenBackup);
            this.GrpPath.Controls.Add(this.BtnOpenInvoice);
            this.GrpPath.Controls.Add(this.BtnBackupPath);
            this.GrpPath.Controls.Add(this.BtnInovicePath);
            this.GrpPath.Controls.Add(this.label5);
            this.GrpPath.Controls.Add(this.TxtBackupPath);
            this.GrpPath.Controls.Add(this.label6);
            this.GrpPath.Controls.Add(this.TxtInvoicePath);
            this.GrpPath.Location = new System.Drawing.Point(271, 6);
            this.GrpPath.Name = "GrpPath";
            this.GrpPath.Size = new System.Drawing.Size(261, 129);
            this.GrpPath.TabIndex = 1;
            this.GrpPath.TabStop = false;
            this.GrpPath.Text = "文件路径";
            // 
            // BtnOpenBackup
            // 
            this.BtnOpenBackup.Location = new System.Drawing.Point(133, 85);
            this.BtnOpenBackup.Name = "BtnOpenBackup";
            this.BtnOpenBackup.Size = new System.Drawing.Size(110, 27);
            this.BtnOpenBackup.TabIndex = 7;
            this.BtnOpenBackup.Text = "打开备份文件夹";
            this.BtnOpenBackup.UseVisualStyleBackColor = true;
            this.BtnOpenBackup.Click += new System.EventHandler(this.BtnOpenFolder_Click);
            // 
            // BtnOpenInvoice
            // 
            this.BtnOpenInvoice.Location = new System.Drawing.Point(20, 85);
            this.BtnOpenInvoice.Name = "BtnOpenInvoice";
            this.BtnOpenInvoice.Size = new System.Drawing.Size(110, 27);
            this.BtnOpenInvoice.TabIndex = 6;
            this.BtnOpenInvoice.Text = "打开发票文件夹";
            this.BtnOpenInvoice.UseVisualStyleBackColor = true;
            this.BtnOpenInvoice.Click += new System.EventHandler(this.BtnOpenFolder_Click);
            // 
            // BtnBackupPath
            // 
            this.BtnBackupPath.Location = new System.Drawing.Point(212, 56);
            this.BtnBackupPath.Name = "BtnBackupPath";
            this.BtnBackupPath.Size = new System.Drawing.Size(31, 25);
            this.BtnBackupPath.TabIndex = 3;
            this.BtnBackupPath.Text = "...";
            this.BtnBackupPath.UseVisualStyleBackColor = true;
            this.BtnBackupPath.Click += new System.EventHandler(this.BtnSelectPath_Click);
            // 
            // BtnInovicePath
            // 
            this.BtnInovicePath.Location = new System.Drawing.Point(212, 27);
            this.BtnInovicePath.Name = "BtnInovicePath";
            this.BtnInovicePath.Size = new System.Drawing.Size(31, 25);
            this.BtnInovicePath.TabIndex = 1;
            this.BtnInovicePath.Text = "...";
            this.BtnInovicePath.UseVisualStyleBackColor = true;
            this.BtnInovicePath.Click += new System.EventHandler(this.BtnSelectPath_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "备份";
            // 
            // TxtBackupPath
            // 
            this.TxtBackupPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtBackupPath.Location = new System.Drawing.Point(55, 57);
            this.TxtBackupPath.Name = "TxtBackupPath";
            this.TxtBackupPath.Size = new System.Drawing.Size(151, 23);
            this.TxtBackupPath.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "发票";
            // 
            // TxtInvoicePath
            // 
            this.TxtInvoicePath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtInvoicePath.Location = new System.Drawing.Point(55, 28);
            this.TxtInvoicePath.Name = "TxtInvoicePath";
            this.TxtInvoicePath.Size = new System.Drawing.Size(151, 23);
            this.TxtInvoicePath.TabIndex = 0;
            // 
            // GrpUsr
            // 
            this.GrpUsr.Controls.Add(this.BtnQrLogin);
            this.GrpUsr.Controls.Add(this.BtnLogin);
            this.GrpUsr.Controls.Add(this.label1);
            this.GrpUsr.Controls.Add(this.CboStatus);
            this.GrpUsr.Controls.Add(this.label7);
            this.GrpUsr.Controls.Add(this.TxtPwd);
            this.GrpUsr.Controls.Add(this.label8);
            this.GrpUsr.Controls.Add(this.TxtUsr);
            this.GrpUsr.Location = new System.Drawing.Point(4, 6);
            this.GrpUsr.Name = "GrpUsr";
            this.GrpUsr.Size = new System.Drawing.Size(261, 129);
            this.GrpUsr.TabIndex = 0;
            this.GrpUsr.TabStop = false;
            this.GrpUsr.Text = "淘宝账号";
            // 
            // BtnQrLogin
            // 
            this.BtnQrLogin.Location = new System.Drawing.Point(182, 85);
            this.BtnQrLogin.Name = "BtnQrLogin";
            this.BtnQrLogin.Size = new System.Drawing.Size(61, 27);
            this.BtnQrLogin.TabIndex = 6;
            this.BtnQrLogin.Text = "扫码";
            this.BtnQrLogin.UseVisualStyleBackColor = true;
            this.BtnQrLogin.Click += new System.EventHandler(this.BtnQrLogin_Click);
            // 
            // BtnLogin
            // 
            this.BtnLogin.Location = new System.Drawing.Point(119, 85);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(61, 27);
            this.BtnLogin.TabIndex = 3;
            this.BtnLogin.Text = "登录";
            this.BtnLogin.UseVisualStyleBackColor = true;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "状态";
            // 
            // CboStatus
            // 
            this.CboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboStatus.FormattingEnabled = true;
            this.CboStatus.Items.AddRange(new object[] {
            "未登录",
            "已登录"});
            this.CboStatus.Location = new System.Drawing.Point(55, 86);
            this.CboStatus.Name = "CboStatus";
            this.CboStatus.Size = new System.Drawing.Size(60, 25);
            this.CboStatus.TabIndex = 2;
            this.CboStatus.SelectedIndexChanged += new System.EventHandler(this.CboStatus_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "密码";
            // 
            // TxtPwd
            // 
            this.TxtPwd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtPwd.Location = new System.Drawing.Point(55, 57);
            this.TxtPwd.Name = "TxtPwd";
            this.TxtPwd.Size = new System.Drawing.Size(188, 23);
            this.TxtPwd.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "账户";
            // 
            // TxtUsr
            // 
            this.TxtUsr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtUsr.Location = new System.Drawing.Point(55, 28);
            this.TxtUsr.Name = "TxtUsr";
            this.TxtUsr.Size = new System.Drawing.Size(188, 23);
            this.TxtUsr.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.SplWeb);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer5);
            this.splitContainer2.Size = new System.Drawing.Size(1784, 961);
            this.splitContainer2.SplitterDistance = 720;
            this.splitContainer2.SplitterWidth = 1;
            this.splitContainer2.TabIndex = 1;
            // 
            // SplWeb
            // 
            this.SplWeb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplWeb.IsSplitterFixed = true;
            this.SplWeb.Location = new System.Drawing.Point(0, 0);
            this.SplWeb.Name = "SplWeb";
            // 
            // SplWeb.Panel1
            // 
            this.SplWeb.Panel1.Controls.Add(this.WebL);
            // 
            // SplWeb.Panel2
            // 
            this.SplWeb.Panel2.Controls.Add(this.WebR);
            this.SplWeb.Size = new System.Drawing.Size(1784, 720);
            this.SplWeb.SplitterDistance = 889;
            this.SplWeb.SplitterWidth = 1;
            this.SplWeb.TabIndex = 0;
            // 
            // WebL
            // 
            this.WebL.ActivateBrowserOnCreation = false;
            this.WebL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WebL.Location = new System.Drawing.Point(0, 0);
            this.WebL.Name = "WebL";
            this.WebL.Size = new System.Drawing.Size(889, 720);
            this.WebL.TabIndex = 0;
            this.WebL.LoadingStateChanged += new System.EventHandler<CefSharp.LoadingStateChangedEventArgs>(this.Web_LoadingStateChanged);
            // 
            // WebR
            // 
            this.WebR.ActivateBrowserOnCreation = false;
            this.WebR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WebR.Location = new System.Drawing.Point(0, 0);
            this.WebR.Name = "WebR";
            this.WebR.Size = new System.Drawing.Size(894, 720);
            this.WebR.TabIndex = 0;
            this.WebR.LoadingStateChanged += new System.EventHandler<CefSharp.LoadingStateChangedEventArgs>(this.Web_LoadingStateChanged);
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.IsSplitterFixed = true;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.TabCtrls);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.LblListCount);
            this.splitContainer5.Panel2.Controls.Add(this.ChbIdentifyStatus);
            this.splitContainer5.Panel2.Controls.Add(this.ChbDataLock);
            this.splitContainer5.Panel2.Controls.Add(this.ChbRunStatus);
            this.splitContainer5.Panel2.Controls.Add(this.BtnStop);
            this.splitContainer5.Panel2.Controls.Add(this.BtnRun);
            this.splitContainer5.Size = new System.Drawing.Size(1784, 240);
            this.splitContainer5.SplitterDistance = 1600;
            this.splitContainer5.TabIndex = 0;
            // 
            // LblListCount
            // 
            this.LblListCount.AutoSize = true;
            this.LblListCount.Location = new System.Drawing.Point(9, 211);
            this.LblListCount.Name = "LblListCount";
            this.LblListCount.Size = new System.Drawing.Size(68, 17);
            this.LblListCount.TabIndex = 4;
            this.LblListCount.Text = "列表总数：";
            // 
            // ChbIdentifyStatus
            // 
            this.ChbIdentifyStatus.AutoSize = true;
            this.ChbIdentifyStatus.Enabled = false;
            this.ChbIdentifyStatus.Location = new System.Drawing.Point(93, 159);
            this.ChbIdentifyStatus.Name = "ChbIdentifyStatus";
            this.ChbIdentifyStatus.Size = new System.Drawing.Size(75, 21);
            this.ChbIdentifyStatus.TabIndex = 3;
            this.ChbIdentifyStatus.Text = "人工验证";
            this.ChbIdentifyStatus.UseVisualStyleBackColor = true;
            // 
            // ChbDataLock
            // 
            this.ChbDataLock.AutoSize = true;
            this.ChbDataLock.Enabled = false;
            this.ChbDataLock.Location = new System.Drawing.Point(11, 183);
            this.ChbDataLock.Name = "ChbDataLock";
            this.ChbDataLock.Size = new System.Drawing.Size(87, 21);
            this.ChbDataLock.TabIndex = 2;
            this.ChbDataLock.Text = "数据锁状态";
            this.ChbDataLock.UseVisualStyleBackColor = true;
            // 
            // ChbRunStatus
            // 
            this.ChbRunStatus.AutoSize = true;
            this.ChbRunStatus.Enabled = false;
            this.ChbRunStatus.Location = new System.Drawing.Point(11, 159);
            this.ChbRunStatus.Name = "ChbRunStatus";
            this.ChbRunStatus.Size = new System.Drawing.Size(75, 21);
            this.ChbRunStatus.TabIndex = 1;
            this.ChbRunStatus.Text = "运行状态";
            this.ChbRunStatus.UseVisualStyleBackColor = true;
            // 
            // FrmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1784, 961);
            this.Controls.Add(this.splitContainer2);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "千牛发票上传助手 v1.0";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_FormClosed);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmMain_KeyDown);
            this.TabCtrls.ResumeLayout(false);
            this.TabpList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvList)).EndInit();
            this.TabpSetting.ResumeLayout(false);
            this.GrpAuto.ResumeLayout(false);
            this.GrpAuto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NudLoopQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudLoopTime)).EndInit();
            this.GrpPath.ResumeLayout(false);
            this.GrpPath.PerformLayout();
            this.GrpUsr.ResumeLayout(false);
            this.GrpUsr.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.SplWeb.Panel1.ResumeLayout(false);
            this.SplWeb.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplWeb)).EndInit();
            this.SplWeb.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            this.splitContainer5.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl TabCtrls;
        private System.Windows.Forms.TabPage TabpList;
        private System.Windows.Forms.DataGridView DgvList;
        private System.Windows.Forms.TabPage TabpSetting;
        private System.Windows.Forms.GroupBox GrpAuto;
        private System.Windows.Forms.NumericUpDown NudLoopTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox GrpPath;
        private System.Windows.Forms.Button BtnBackupPath;
        private System.Windows.Forms.Button BtnInovicePath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtBackupPath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtInvoicePath;
        private System.Windows.Forms.GroupBox GrpUsr;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CboStatus;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtPwd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtUsr;
        private System.Windows.Forms.Button BtnRun;
        private System.Windows.Forms.Button BtnStop;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.SplitContainer SplWeb;
        private CefSharp.WinForms.ChromiumWebBrowser WebL;
        private CefSharp.WinForms.ChromiumWebBrowser WebR;
        private System.Windows.Forms.Button BtnOpenInvoice;
        private System.Windows.Forms.Button BtnOpenBackup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown NudLoopQty;
        private System.Windows.Forms.CheckBox ChbIdentifyStatus;
        private System.Windows.Forms.CheckBox ChbDataLock;
        private System.Windows.Forms.CheckBox ChbRunStatus;
        private System.Windows.Forms.Button BtnQrLogin;
        private System.Windows.Forms.DataGridViewTextBoxColumn FStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn FQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn FOrderNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn FPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn FAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn FAmountDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn FOLAmount;
        private System.Windows.Forms.Label LblListCount;
    }
}