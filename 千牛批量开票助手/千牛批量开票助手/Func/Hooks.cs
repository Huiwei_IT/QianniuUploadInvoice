﻿using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Win64MKC;
using static 千牛批量开票助手.Func.Threads;

namespace 千牛批量开票助手.Func
{
    internal class Hooks
    {
        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "FindWindowEx")]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, string lParam);

        private static readonly int WM_SETTEXT = 0x000C;           // 发送文本消息
        public static readonly int WM_LBUTTONDOWN = 0x0201;       // 发送左键按下事件
        public static readonly int WM_LBUTTONUP = 0x0202;         // 发送左键抬起事件

        public static readonly IntPtr VK_LBUTTON = new IntPtr(1); // 鼠标左键虚拟键值

        /// <summary>
        /// 句柄对话框上传文件
        /// </summary>
        public static bool DialogUploadSingleFile(string path)
        {
            IntPtr dialogHwnd = IntPtr.Zero;
            double i = 0;
            while (i < 120)
            {
                if (!RUN_STATUS) return false;
                dialogHwnd = FindWindow("#32770", "打开");
                if (dialogHwnd != IntPtr.Zero) break;
                i += 0.1;
                Thread.Sleep(100);
            }

            if (i >= 120) return false;

            if (dialogHwnd != IntPtr.Zero)
            {
                IntPtr dialogCboExHwnd = FindWindowEx(dialogHwnd, IntPtr.Zero, "ComboBoxEx32", null);
                IntPtr dialogCboHwnd = FindWindowEx(dialogCboExHwnd, IntPtr.Zero, "ComboBox", null);
                IntPtr dialogEditHwnd = FindWindowEx(dialogCboHwnd, IntPtr.Zero, "Edit", null);

                IntPtr dialogBtnOpenHwnd = FindWindowEx(dialogHwnd, IntPtr.Zero, "Button", "打开(&O)");

                if (dialogEditHwnd != IntPtr.Zero & dialogBtnOpenHwnd != IntPtr.Zero)
                {
                    // 延时 0.5 秒, 发送文件路径
                    Thread.Sleep(500);
                    SendMessage(dialogEditHwnd, WM_SETTEXT, IntPtr.Zero, path);
                    Thread.Sleep(100);
                    SendMessage(dialogBtnOpenHwnd, WM_LBUTTONDOWN, VK_LBUTTON, null);
                    SendMessage(dialogBtnOpenHwnd, WM_LBUTTONUP, IntPtr.Zero, null);
                }

                // 等待打开对话框关闭
                i = 0;
                while (i < 120)
                {
                    if (!RUN_STATUS) return false;
                    dialogHwnd = FindWindow("#32770", "打开");
                    if (dialogHwnd == IntPtr.Zero) break;
                    i += 0.1;
                    Thread.Sleep(100);
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// 设置右侧浏览器焦点
        /// </summary>
        public static void SetWebObjFocus(Form frmMain, ChromiumWebBrowser webObj)
        {
            Point point = new Point(-1, -1);
            frmMain.Invoke(new Action(() => { point = webObj.PointToScreen(webObj.Location); }));

            Mouse mouse = new Mouse();
            mouse.Move(point.X + 5, point.Y + 5);
            mouse.LeftClick();
        }
    }
}
